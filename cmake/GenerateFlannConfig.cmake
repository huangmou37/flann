# Generate the FlannConfig.cmake file in the build tree.
# The file tells external projects how to use Flann.

# Settings specific to build trees
#
#
set(Flann_USE_FILE_CONFIG ${flann_BINARY_DIR}/UseFlann.cmake)

#Search for header directories
set(Flann_INCLUDE_DIRS_CONFIG
  ${flann_SOURCE_DIR}/src/cpp
  )

set(Flann_LIBRARY_DIRS_CONFIG
  ${LIBRARY_OUTPUT_PATH}
  )
set(Flann_BINARY_DIR_CONFIG
  ${flann_BINARY_DIR}
  )
set(Flann_LIBRARIES_CONFIG
  ${flann_LIBRARIES}
  )

configure_file(
  ${flann_SOURCE_DIR}/cmake/FlannConfig.cmake.in
  ${flann_BINARY_DIR}/FlannConfig.cmake
  @ONLY IMMEDIATE
  )

configure_file(
  ${flann_SOURCE_DIR}/cmake/UseFlann.cmake.in
  ${Flann_USE_FILE_CONFIG}
  @ONLY IMMEDIATE
  )

